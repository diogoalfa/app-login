import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {StorageService} from '../services/storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router,
              private storageService: StorageService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (state.url === '/login') {
      if (this.storageService.isAuthenticated()) {
        this.router.navigate(['dashboard']);
        return false;
      }
    } else {
      if (!this.storageService.isAuthenticated()) {
        this.router.navigate(['login']);
        return false;
      }
    }

    if (state.url === '/resultadoCotizacion') {
      if (!sessionStorage.getItem('respCotizacion')) {
        this.router.navigate(['dashboard']);
        return false;
      }
    }

    return true;
  }

}
