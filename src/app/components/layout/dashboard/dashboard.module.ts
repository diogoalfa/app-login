import {CommonModule, DatePipe} from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { MatGridListModule } from '@angular/material/grid-list';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { ComponentsModule } from '../../ui/components/components.module';
import { DashboardComponent } from './dashboard.component';
import {
  MatDatepickerModule,
  MatButtonModule,
  MatCardModule,
  MatIconModule,
  MatTableModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatProgressSpinnerModule,
  MatProgressBarModule,
  MatNativeDateModule, MatExpansionModule, MatStepperModule, MatCheckboxModule, MatRadioModule
} from '@angular/material';
import {FilterPipe} from '../../../pipes/filter.pipe';
import {Ng2Rut} from 'ng2-rut';

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MatGridListModule,
    ReactiveFormsModule,
    ComponentsModule,
    MatDatepickerModule,
    MatCardModule,
    MatCardModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatIconModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatNativeDateModule,
    FlexLayoutModule.withConfig({addFlexToParent: false}),
    MatExpansionModule,
    Ng2Rut,
    MatStepperModule,
    MatCheckboxModule,
    MatRadioModule
  ],
    declarations: [
        DashboardComponent,
        FilterPipe
    ],
  providers: [DatePipe, FilterPipe]
})
export class DashboardModule {}
