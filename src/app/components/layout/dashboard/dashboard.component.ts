import { Component, OnInit }    from '@angular/core';
import {Category} from "../../../core/model/category";
import {CodeLineProd} from "../../../core/model/code-line-prod";


@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit{
    loading             : boolean;
    loadingDashboard    : boolean;
    questions: any[];
    categories:Category[];
    lineAndProd:CodeLineProd;


    constructor(){
        this.loadingDashboard = true;
        this.questions = [];
    }

    ngOnInit() {
        let vm = this;
        
        vm.loading = true;
    }

  /**
   *
   * @param questions
   */
  loadQuestion(questions: any[]){
      this.questions = questions;
  }

  loadCategories(categories:Category[]){
    this.categories = categories;
  }

  loadCodLineAndProd(lineAndProd:CodeLineProd){
    this.lineAndProd=lineAndProd;
  }

}
