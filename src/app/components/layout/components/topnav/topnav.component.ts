import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { UserLogin } from '../../../../core/model/user-login';
import { StorageService} from '../../../../services/storage.service';

@Component({
    selector: 'app-topnav',
    templateUrl: './topnav.component.html',
    styleUrls: ['./topnav.component.scss']
})
export class TopnavComponent implements OnInit {
    public pushRightClass: string;
    public user: string;

    constructor(
        public  router: Router,
        private storageService: StorageService
    ) {
        this.router.events.subscribe(val => {
            if (val instanceof NavigationEnd && window.innerWidth <= 992 && this.isToggled()) {
                this.toggleSidebar();
            }
        });
    }

    ngOnInit() {

        this.getUser();
        this.pushRightClass   = 'push-right';
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    onLoggedout() {
        this.storageService.logout();
        this.router.navigate(['/login']);
    }

    changeLang(language: string) {
        //this.translate.use(language);
    }

    getUser(): void {
        const userSession: UserLogin = this.storageService.getCurrentUser();
        this.user = userSession.isLoggedin ? userSession.userName : '';
    }
}
