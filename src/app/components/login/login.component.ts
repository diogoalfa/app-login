import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material';
import {LoadingDialogComponent} from '../layout/components/loading-dialog/loading-dialog.component';
import {LoginService} from '../../services/login.service';
import {map} from 'rxjs/operators';
import {UserLogin} from '../../core/model/user-login';
import {StorageService} from '../../services/storage.service';
import {Session} from '../../core/model/session';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  userForm = new FormGroup({
    userName: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required)
  });
  disableSubmit = false;

  constructor(
    private router: Router,
    public dialog: MatDialog,
    private ls: LoginService,
    private storageService: StorageService) { }

  ngOnInit() {
  }

  onSubmit() {
    this.disableSubmit = true;
    const dialogRef = this.dialog.open(LoadingDialogComponent, {
      width: '250px',
      disableClose: true
    });
    this.ls.findUserBroker(this.userForm.controls['userName'].value)
      .pipe(map(response => response.data))
      .subscribe(broker => {
        const session: Session = new Session();
        session.token = 'ASDAasdasd···$2233';
        const user: UserLogin = new UserLogin();
        user.isLoggedin = true;
        user.userName = broker.username;
        user.fullName = broker.fullName;
        user.userCode = broker.code;
        session.user = user;
        this.storageService.setCurrentSession(session);
        this.router.navigate(['dashboard']);
        dialogRef.close();
      });
  }

}
