import { Component, Inject }              from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA }  from '@angular/material';
import {Confirm} from "../../../../core/model/confirm";

@Component({
  templateUrl : './confirm.component.html',
  styleUrls   : ['./confirm.component.scss']
})
export class ConfirmComponent {

  constructor(
    public dialogRef: MatDialogRef<ConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Confirm
  ){}

}
