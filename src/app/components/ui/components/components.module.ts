import { NgModule }                 from '@angular/core';
import { CommonModule }             from '@angular/common';
import { NotificationsComponent }   from './notifications/notifications.component';
import { MatIconModule, MatDialogModule, MatButtonModule } from '@angular/material';
import { ConfirmComponent }         from './confirm/confirm.component';

@NgModule({
  declarations: [
    NotificationsComponent,
    ConfirmComponent
  ],
  imports: [
    CommonModule,
    MatIconModule,
    MatDialogModule,
    MatButtonModule
  ],
  exports:[
    NotificationsComponent,
    ConfirmComponent
  ],
  entryComponents: [
    ConfirmComponent
  ]
})
export class ComponentsModule { }
