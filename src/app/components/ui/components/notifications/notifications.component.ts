import { Component,  OnInit, Input, OnChanges }  from '@angular/core';
import { Notification }               from '../../../../core/model/notification';

@Component({
  selector    : 'app-notifications',
  templateUrl : './notifications.component.html',
  styleUrls   : ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit, OnChanges {

  @Input() notification : Notification;

  constructor(){}

  ngOnInit() {
    let vm = this;

    if (vm.notification && vm.notification.timeAlive) {
      setTimeout(
        function(){
          vm.notification = null;
        },
        vm.notification.timeAlive
      );
    }
  }

  ngOnChanges(changes) {
    let vm = this;

    if (changes && changes.notification) {
      if (vm.notification && vm.notification.timeAlive) {
        setTimeout(
          function(){
            vm.notification = null;
          },
          vm.notification.timeAlive
        );
      }
    }
  }

}
