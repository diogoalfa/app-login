import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {RespCotizacionVehiculo} from '../core/model/resp-cotizacion-vehiculo';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  private static readonly cotizadorVehiculos = environment.apiEndpointCotizadorVehiculos;

  constructor(private http: HttpClient) { }

  cotizarVehiculo(form: any): Observable<RespCotizacionVehiculo> {
    return this.http.post<RespCotizacionVehiculo>(RestService.cotizadorVehiculos, form);
  }

  // TODO:cambiar tipo respuesta solo mockup
  cotizarHogar(): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json; charset=utf-8');
    return this.http.post<RespCotizacionVehiculo>(
          'http://localhost:8080/advanced-generic-quote/v1.0.0/generic/home',
               JSON.parse('{}'),
      {headers: headers}
      );
  }

}
