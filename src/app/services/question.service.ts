import { Injectable, Output} from '@angular/core';
import { DropdownQuestion } from '../questions/question-dropdown';
import { TextboxQuestion } from '../questions/question-textbox';
import { QuestionBase } from '../questions/question-base';
import { QuestionRut} from "../questions/question-rut";
import { DatePickerQuestion} from "../questions/question-datepicker";
import {QuestionSelectDependent} from "../questions/question-select-dependent";
import {QuestionSelectChild} from "../questions/question-selectchild";
import {ChecklistQuestion} from '../questions/question-checklist';
import {RadioQuestion} from '../questions/question-radio';
import {MapQuestion} from '../questions/question-map';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  // TODO: get from a remote source of question metadata
  // TODO: make asynchronous
  questions: QuestionBase<any>[];

  /**
   * Función para crear objeto question para mostrar en el form dynamic
   *
   * @param mapProperties
   */
  createQuestionT(mapProperties:Map<string,any>): QuestionBase<any> {
    let questionT: QuestionBase<any>;
    let controlType: string = mapProperties.get('controlType');
    if (controlType !== undefined && controlType !== null) {
      switch (controlType) {
        case "textbox":
          questionT = new TextboxQuestion({
            key: mapProperties.get('key'),
            label: mapProperties.get('label'),
            type: mapProperties.get('type'),
            required: mapProperties.get('required') === 'true',
            order: mapProperties.get('order'),
            categoryId: mapProperties.get('categoryId'),
            entityName: mapProperties.get('entityName') || '',
            minLength: parseFloat(mapProperties.get('minLength')),
            maxLength: parseFloat(mapProperties.get('maxLength')),
            pattern: mapProperties.get('pattern'),
            positiveNum: mapProperties.get('positiveNum') === 'true',
            display: mapProperties.get('display') ? JSON.parse(mapProperties.get('display')) : null,
            depend: mapProperties.get('depend'),
            min: parseFloat(mapProperties.get('min'))
          });
          break;
        case "rut":
          questionT = new QuestionRut({
            key: mapProperties.get('key'),
            label: mapProperties.get('label'),
            value: mapProperties.get('value'),
            required: mapProperties.get('required') === 'true',
            validateRut: mapProperties.get('validateRut') === 'true',
            order: mapProperties.get('order'),
            categoryId: mapProperties.get('categoryId'),
            entityName: mapProperties.get('entityName') || '',
            minLength: parseFloat(mapProperties.get('minLength')),
            maxLength: parseFloat(mapProperties.get('maxLength'))
          });
          break;
        case "datepicker":
          questionT = new DatePickerQuestion({
            key: mapProperties.get('key'),
            label: mapProperties.get('label'),
            type: mapProperties.get('type'),
            required: mapProperties.get('required') === 'true',
            order: mapProperties.get('order'),
            categoryId: mapProperties.get('categoryId'),
            entityName: mapProperties.get('entityName') || ''
          });
          break;
        case "selectdependent":
          questionT = new QuestionSelectDependent(
            {
              key: mapProperties.get('key'),
              label: mapProperties.get('label'),
              required: mapProperties.get('required') === 'true',
              order: mapProperties.get('order'),
              options: typeof mapProperties.get('options') === 'string' ? JSON.parse(mapProperties.get('options')) : mapProperties.get('options'),
              isParent: mapProperties.get('isParent') === 'true',
              keyChild: mapProperties.get('keyChild') || '',
              isChild: mapProperties.get('isChild') === 'true',
              keyParent: mapProperties.get('keyParent') || '',
              entityNameChild: mapProperties.get('entityNameChild') || '',
              categoryId: mapProperties.get('categoryId'),
              entityName: mapProperties.get('entityName') || ''
            }
          );
          break;
        case "dropdown":
          questionT = new DropdownQuestion({
            key: mapProperties.get('key'),
            label: mapProperties.get('label'),
            required: mapProperties.get('required') === 'true',
            order: mapProperties.get('order'),
            categoryId: mapProperties.get('categoryId'),
            options: typeof mapProperties.get('options') === 'string' ? JSON.parse(mapProperties.get('options')) : mapProperties.get('options'),
            entityName: mapProperties.get('entityName') || ''
          });
          break;
        case "checklist":
          questionT = new ChecklistQuestion({
            key: mapProperties.get('key'),
            label: mapProperties.get('label'),
            required: mapProperties.get('required') === 'true',
            order: mapProperties.get('order'),
            categoryId: mapProperties.get('categoryId'),
            items: typeof mapProperties.get('items') === 'string' ? JSON.parse(mapProperties.get('items')) : mapProperties.get('items')
          });
          break;
        case "radio":
          questionT = new RadioQuestion({
            key: mapProperties.get('key'),
            label: mapProperties.get('label'),
            required: mapProperties.get('required') === 'true',
            order: mapProperties.get('order'),
            categoryId: mapProperties.get('categoryId'),
            items: typeof mapProperties.get('items') === 'string' ? JSON.parse(mapProperties.get('items')) : mapProperties.get('items'),
            display: mapProperties.get('display') ? JSON.parse(mapProperties.get('display')) : null,
            depend: mapProperties.get('depend')
          });
          break;
        case "map":
          questionT = new MapQuestion({
            key: mapProperties.get('key'),
            label: mapProperties.get('label'),
            required: mapProperties.get('required') === 'true',
            order: mapProperties.get('order'),
            categoryId: mapProperties.get('categoryId')
          });
          break;
        default:
          break;
      }
    }
    return questionT;
  }

  createQuestion(
    key:string,
    label:string,
    type:string,
    order:number,
    required:boolean,
    options:any[],
    controlType:string
    ){
    let questionT: QuestionBase<any>;
    switch (controlType) {
      case "textbox":
        questionT = new TextboxQuestion({
          key: key,
          label: label,
          type: type,
          required:required,
          order: order
        });
        break;
      case "rut":
        questionT =  new QuestionRut({
          key: key,
          label: label,
          value: '',
          required: required,
          order: order
        });
        break;
      case "dropdown":
        questionT=new DropdownQuestion({
          key: key,
          label: label,
          options: options,
          required:required,
          order: order
        });
        break;
      case "datepicker":
        questionT=new DatePickerQuestion({
          key: key,
          label: label,
          type: type,
          required:required,
          order: order
        });
        break;
      case "selectdependent":

        break;
      default:
        break;
    }
    return questionT;
  }
}
