import { Injectable } from '@angular/core';
import {RestService} from "./rest.service";
import {CotGenLibBodyRequest} from "./model/cot-gen-lib-body-request";
import {GeneralDataService} from './general-data.service';
import {Router} from '@angular/router';
import {MatDialog, MatDialogRef} from '@angular/material';
import {LoadingDialogComponent} from '../components/layout/components/loading-dialog/loading-dialog.component';
import {DatePipe} from '@angular/common';
import {RestmanagerService} from './restmanager.service';
import {StorageService} from './storage.service';
import {Paramtree} from '../core/model/paramtree';
import {map} from 'rxjs/operators';
import {Restdescription} from '../core/model/restdescription';
import {Observable, Observer, Subject} from 'rxjs';

/**
 * Servicio para orquestar las llamadas a los webservice de tarificación
 */
@Injectable({
  providedIn: 'root'
})
export class OrchestratorQuoteService {
  private bodyRequestTemp: any;
  private codLine: string;
  private product: string;
  private broker: string;
  private requestBody$ = new Subject<object>();
  public requestBody: object;

  constructor(
    private rs: RestService,
    private gds: GeneralDataService,
    private router: Router,
    private datePipe: DatePipe,
    private restManagerService: RestmanagerService,
    private storageService: StorageService
  ) { }

  goToQuote(line: string, product: string, bodyRequest: any, dialog: MatDialogRef<LoadingDialogComponent>) {
    this.broker = this.storageService.getCurrentUser().userCode;
    this.codLine = line;
    this.restManagerService.getRestDescription(product, this.broker)
      .pipe(map(response => response.data))
      .subscribe(response => {
        console.log('this.restManagerService.getRestDescription :');
        console.log(response);
        const urlRestDescription: Restdescription = response.find( el => el.paramKey === 'url' );
        this.restManagerService.getRestManagerQuote(
                                  urlRestDescription.paramValue,
                                  '',
                                  bodyRequest
                                )
                                .subscribe( responseQuote => {
                                  if (responseQuote.codeStatus === 200) {
                                    this.gds.setRespCotizacionVehiculo(responseQuote.data);
                                    sessionStorage.setItem('respCotizacion', JSON.stringify(responseQuote.data));
                                    this.router.navigate(['/resultadoCotizacion']);
                                  } else {
                                      alert(responseQuote.status);
                                  }
                                  dialog.close();
                                });
    });
  }

  /*
  function asyncAction() {
  var promise = new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log("Async is done!");
      resolve();
    }, 1500);
  });
  return promise;
}
   */
  createBodyRequestV2(codLine: string, product: string, formValues: any): Promise<object> {
    this.broker = this.storageService.getCurrentUser().userCode;
    this.codLine = codLine;
    this.product = product;
    const promise = new Promise((resolve, reject) => {
      const jsonObjectTree: object = new Object();
      const parametersArr: any[] = formValues.formArray ? formValues.formArray : [];
      this.restManagerService.getRestParameterTree(this.product, this.broker)
          .pipe(map(response => response.data))
          .subscribe(paramTreeList => {
            const nodesRoot: Paramtree[] = this.getNodeRoot(paramTreeList);
            nodesRoot.forEach(paramTreeRoot  => {
              if (paramTreeRoot.paramValue != null) {
                jsonObjectTree[paramTreeRoot.paramKey] = paramTreeRoot.paramValue;
              } else {
                jsonObjectTree[paramTreeRoot.paramKey] = this.getNodesFromRootNode(
                  paramTreeList,
                  parametersArr,
                  paramTreeRoot
                );
              }
            });
            resolve(jsonObjectTree);
          });
    });
    return promise;
  }

  addElementoToBodyRequest(requestBodyTemp: object) {
     this.requestBody = requestBodyTemp;
     this.requestBody$.next(this.requestBody);
  }

  getBodyRequest(): Observable<object> {
    return this.requestBody$.asObservable();
  }

  getNodeRoot(paramTreeList: Paramtree[]): any {
    return paramTreeList.filter(paramTree => paramTree.paramTreeParent == null);
  }

  getNodesFromRootNode(paramTreeList: Paramtree[], formValues: any[], paramTree: Paramtree): any {
    let jsonTemp: any;
    const nodesChild: Paramtree[] = paramTreeList
      .filter(paramTreeTemp => paramTreeTemp.paramTreeParent != null && paramTreeTemp.paramTreeParent.id === paramTree.id);
    if (paramTree.question != null) {
      formValues.forEach(value => {
        if (value[paramTree.question.paramKey]) {
          jsonTemp = value[paramTree.question.paramKey];
        }
      });
    } else {
      nodesChild.forEach(paramTreeTemp => {
        if (paramTreeTemp.paramValue != null) {
          jsonTemp =  new Object();
          jsonTemp[paramTreeTemp.paramKey] = paramTreeTemp.paramValue;
        } else {
          this.getNodesFromRootNode(paramTreeList, formValues, paramTreeTemp);
        }
      });
    }
    return jsonTemp;
  }

  createBodyRequest(codLine: string, product: string, formValues: any) {
    this.codLine = codLine;
    this.product = product;
    if (this.codLine === '6') {
      const parametersArr: any[] = formValues.formArray ? formValues.formArray : [];
      console.log('parametersArr');
      console.log(parametersArr);
      const cotGenLibBodyRequest: CotGenLibBodyRequest = new CotGenLibBodyRequest();
      parametersArr.forEach(parameters => {
        if (parameters['rutCorredor']) {cotGenLibBodyRequest.rutCorredor = this.cortarRutParaCotizadorV(parameters['rutCorredor']) || '';};
        if (parameters['rutContratante']) {cotGenLibBodyRequest.rutContratante  = this.cortarRutParaCotizadorV(parameters['rutContratante']) || '';}
        if (parameters['rutSponsor'])  {cotGenLibBodyRequest.rutSponsor  = this.cortarRutParaCotizadorV(parameters['rutSponsor']) || '';}
        if (parameters['digContratante']) {cotGenLibBodyRequest.digContratante  = parameters['digContratante'] || '';}
        if (parameters['rutAsegurado']) {cotGenLibBodyRequest.rutAsegurado  = this.cortarRutParaCotizadorV(parameters['rutAsegurado']) || '';};
        if (parameters['tipoVehiculo'])  {cotGenLibBodyRequest.tipoVehiculo  = parameters['tipoVehiculo'] || '';}
        if (parameters['marcaVehiculo'] ) {cotGenLibBodyRequest.marcaVehiculo = parameters['marcaVehiculo'] || '';}
        if (parameters['modeloVehiculo']) {cotGenLibBodyRequest.modeloVehiculo  = parameters['modeloVehiculo'] || '';}
        if (parameters['anoVehiculo'] ) {cotGenLibBodyRequest.anoVehiculo  = parameters['anoVehiculo'] || '';}
        if (parameters['fechaNacimiento']) {cotGenLibBodyRequest.fechaNacimiento  = this.datePipe.transform(
          parameters['fechaNacimiento'] || '',
          'yyyyMMdd'
        );}
        if (parameters['comuna']) {cotGenLibBodyRequest.comuna  = parameters['comuna'] || '';}
        if (this.product) {cotGenLibBodyRequest.producto = this.product;}
        // cotGenLibBodyRequest.fechaNacimiento  = parameters['fechaNacimiento'] || '';
        // cotGenLibBodyRequest.fechaNacimiento  = '19910307';
        if (parameters['nroMotor']) {cotGenLibBodyRequest.nroMotor  = parameters['nroMotor'] || '';}
        if (parameters['vIN']) {cotGenLibBodyRequest.vIN  = parameters['vIN'] || '';}
        if (parameters['usoDestino']) {cotGenLibBodyRequest.usoDestino  = parameters['usoDestino'] || 'usoDestino';}
        // cotGenLibBodyRequest.appKey  = parameters['appKey'] || '';
        cotGenLibBodyRequest.appKey  = 'u04T6FSfokxza3BLNIVM3E6O8qfZKv7KmFvOhppq32oIlq5xyaqwhQvU_NzI9VuXAwAK60_1HSkYj4LkoWKh9g==' ;
        if (parameters['diasVigencia']) {cotGenLibBodyRequest.diasVigencia  = parameters['diasVigencia'] || '';}
        if (parameters['formaPago']) {cotGenLibBodyRequest.formaPago  = parameters['formaPago'] || '';}
        if (parameters['cantidadCuotas']) {cotGenLibBodyRequest.cantidadCuotas  = parameters['cantidadCuotas'] || '';}
        // cotGenLibBodyRequest.mapValues  = parameters['mapValues'] || {};
        cotGenLibBodyRequest.mapValues  = {COMPANIA_ID: '0'};
        this.bodyRequestTemp = cotGenLibBodyRequest;
      });
      this.bodyRequestTemp = cotGenLibBodyRequest;
    } else {
      this.bodyRequestTemp = formValues;
    }
    return this;
  }

  cortarRutParaCotizadorV(rut: string) {
    return rut.replace('-', '')
              .replace('.', '')
              .replace('.', '')
              .substr(0, rut.length - 1);
  }

}
