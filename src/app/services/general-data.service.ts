import { Injectable } from '@angular/core';
import { GenericVehicleQuoteService } from "./generic-vehicle-quote.service";
import { RespCotizacionVehiculo} from '../core/model/resp-cotizacion-vehiculo';
import { Observable} from "rxjs";
import { HttpClient, HttpParams} from "@angular/common/http";
import { environment} from "../../environments/environment";
import { JsonResponse} from "./model/JsonResponse";
import { map} from "rxjs/operators";
import { KeyValuePairObj} from "../core/model/KeyValuePairObj";

@Injectable({
  providedIn: 'root'
})
/**
 * Servicio creado para consultar datos generales y datos dependientes de otros.
 */
export class GeneralDataService {

  private static readonly urlApiAdvancedGenericQuote = environment.apiEndpointAdvancedGenericQuote;

  private resutadoCotiacion: RespCotizacionVehiculo;

  constructor(private http: HttpClient,
              private gvqs: GenericVehicleQuoteService) { }

  getDataKeyValue(entityName: string, keyFilter: string):KeyValuePair[] {
    let arrayTemp:any[];
    switch (entityName) {
      case "modelo":
        arrayTemp = this.gvqs.getModelo(keyFilter);
        break;
      case "marca":
        arrayTemp = this.gvqs.getMarca();
        break;
      case "comuna":
        this.getEntityWithKeyValue("comuna").pipe(
          map(response=>response.data)
        ).subscribe(response => {
          return response;
        });
        break;
      default:
        break;
    }
    return arrayTemp;
  }

  setRespCotizacionVehiculo(item: any): void {
    this.resutadoCotiacion = item;
  }

  getRespCotizacionVehiculo(): RespCotizacionVehiculo {
    return this.resutadoCotiacion;
  }

  getEntityWithKeyValue(
            entityName: string,
            paramsQuery?: KeyValuePairObj[]
  ): Observable<JsonResponse<KeyValuePairObj[]>> {
    const METHOD  = '/generic/' + entityName;
    const URL     = GeneralDataService.urlApiAdvancedGenericQuote;
    if (paramsQuery) {
      let params     = new HttpParams();
      paramsQuery.forEach(value => {
        params =  params.append(value.key, value.value.toString());
      });
      return this.http.get<JsonResponse<KeyValuePairObj[]>>(URL + METHOD, { params: params});
    } else {
      return this.http.get<JsonResponse<KeyValuePairObj[]>>(URL + METHOD);
    }
  }

}
