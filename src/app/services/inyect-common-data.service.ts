import { Injectable } from '@angular/core';
import {QuestionBase} from "../questions/question-base";
import {DropdownQuestion} from "../questions/question-dropdown";
import {AdvancedGenericQuoteClientService} from "./advanced-generic-quote-client.service";
import {Line} from "../core/model/line";
import {Observable, Subject, Subscription} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class InyectCommonDataService {

  constructor(private agqc:AdvancedGenericQuoteClientService) { }


  getProducts(lineKey:string){
    let optionsTemp: {key: string, value: string}[] = [];
    switch (lineKey) {
      case "0":
        optionsTemp = [];
        break;
      case "6":
        optionsTemp =[
          {key: '989', value: 'Full Worksite Individual'},
          {key: '993', value: 'Full Worksite Colectivo'},
          {key: '968', value: 'Derco'}
        ];
        break;
      case "20":
        optionsTemp =[
          {key: '731', value: 'Hipotecario Colectivo'},
          {key: '660', value: 'Incendio Banchile'},
          {key: '791', value: 'Hogar Individual'},
          {key: '739', value: 'Incendio Ligero'},
          {key: '790', value: 'Incendio Comunidades'},
          {key: '801', value: 'Hogar Falabella'},
          {key: '799', value: 'Hogar Colectivo'}
        ];
        break;
      default:
        break;
    }
    let question: QuestionBase<any> =
      new DropdownQuestion({
        key: 'product',
        label: 'Products',
        options: optionsTemp,
        order: 2
      });
    return question;
  }

  getPlans(productKey:number){
    let optionsTemp: {key: string, value: string}[] = [];
    switch (productKey) {
      case 993:
        optionsTemp = [
          {key:'561',value:'GAC FORUM COMERCIAL CON EXCLUSIVIDAD'},
          {key:'567',value:'Security PT Basico'}
        ];
        break;
      case 989:
          optionsTemp = [
            {key:'561',value:'Security PT Basico One'},
            {key:'567',value:'Security PT Basico'}
          ];
        break;
      case 965:
        optionsTemp = [
          {key:'561',value:'GAC FORUM COMERCIAL CON EXCLUSIVIDAD'},
          {key:'567',value:'Security PT Basico'}
        ];
      case 969:
        optionsTemp = [
          {key:'198',value:'Venta Nueva Bi-Anual'},
          {key:'199',value:'Venta Nueva Tri-Anual'}
        ];
        break;
      default:
        optionsTemp = [];
        break;
    }
    let question: QuestionBase<any> =
      new DropdownQuestion({
        key: 'plans',
        label: 'Plans',
        options: optionsTemp,
        order: 3
      });
    return question;
  }
}
