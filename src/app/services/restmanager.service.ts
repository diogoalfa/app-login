import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {JsonResponse} from './model/JsonResponse';
import {catchError} from 'rxjs/operators';
import {Restendpoint} from '../core/model/restendpoint';
import {Restdescription} from '../core/model/restdescription';
import {Paramtree} from '../core/model/paramtree';
import {KeyValuePairObj} from '../core/model/KeyValuePairObj';

@Injectable({
  providedIn: 'root'
})
export class RestmanagerService {

  private static readonly  urlApiAdvancedGenericQuote: string = environment.apiEndpointAdvancedGenericQuote;

  constructor(private http: HttpClient) { }

  public getRestEndpoint(codeProduct: string, codBroker: string): Observable<JsonResponse<Restendpoint>> {
    const METHOD  = '/restmanager';
    const URL     = RestmanagerService.urlApiAdvancedGenericQuote;
    const Http    = this.http.get<JsonResponse<Restendpoint>>(
      URL + METHOD,
      { params: new HttpParams().set('product', codeProduct).set('broker', codBroker) }
    );
    return Http.pipe(
      catchError(error => {
        return throwError(error);
      })
    );
  }

  public getRestDescription(codeProduct: string, codBroker: string): Observable<JsonResponse<Restdescription[]>> {
    const METHOD  = '/restmanager/description';
    const URL     = RestmanagerService.urlApiAdvancedGenericQuote;
    const Http    = this.http.get<JsonResponse<Restdescription[]>>(
      URL + METHOD,
      { params: new HttpParams().set('product', codeProduct).set('broker', codBroker) }
    );
    return Http.pipe(
      catchError(error => {
        return throwError(error);
      })
    );
  }

  public getRestParameterTree(codeProduct: string, codBroker: string): Observable<JsonResponse<Paramtree[]>> {
    const METHOD  = '/restmanager/parameter';
    const URL     = RestmanagerService.urlApiAdvancedGenericQuote;
    const Http    = this.http.get<JsonResponse<Paramtree[]>>(
      URL + METHOD,
      { params: new HttpParams().set('product', codeProduct).set('broker', codBroker) }
    );
    return Http.pipe(
      catchError(error => {
        return throwError(error);
      })
    );
  }

  public getRestManagerQuote(
                      url: string,
                      method: string,
                      body: any
                      ): Observable<JsonResponse<any>> {
    const METHOD  = method;
    const URL     = url + METHOD;
    const Http = this.http.post<any>(URL, body);
    return Http.pipe(
      catchError(error => {
        return throwError(error);
      })
    );
  }

}
