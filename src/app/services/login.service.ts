import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {JsonResponse} from './model/JsonResponse';
import {catchError} from 'rxjs/operators';
import {Broker} from '../core/model/broker';
import {UserLogin} from '../core/model/user-login';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private static readonly  urlApiAdvancedGenericQuote: string = environment.apiEndpointAdvancedGenericQuote;

  constructor(private http: HttpClient) { }

  public findUserBroker(username: string): Observable<JsonResponse<Broker>> {
    const METHOD  = '/login/user';
    const URL     = LoginService.urlApiAdvancedGenericQuote;
    const Http    = this.http.get<JsonResponse<Broker>>(
      URL + METHOD,
      { params: new HttpParams().set('user', username) }
      );
    return Http.pipe(
      catchError(error => {
        return throwError(error);
      })
    );
  }

}
