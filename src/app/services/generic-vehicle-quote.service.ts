import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GenericVehicleQuoteService {

  constructor() { }

  getMarca(){
    return [
      {key:'21',value:'FORD'},
      {key:'78',value:'NISSAN'},
      {key:'61',value:'SUZUKI'}];
  }

  getModelo(codMarca:string){
    let modeloTemp:any[];
    modeloTemp = [
      {key:'101',value:'CERVO',keyParent:'61'},
      {key:'103',value:'FRONTE',keyParent:'61'},
      {key:'105',value:'BALENO',keyParent:'61'},
      {key:'107',value:'ALTO',keyParent:'61'},
      {key:'161',value:'AERIO',keyParent:'61'},
      {key:'204',value:'MASTERVAN',keyParent:'61'},
      {key:'227',value:'LIANA',keyParent:'61'},
      {key:'401',value:'CARRY ALL',keyParent:'61'},
      {key:'403',value:'IGNIS',keyParent:'61'},
      {key:'404',value:'SX4',keyParent:'61'},
      {key:'406',value:'APV',keyParent:'61'},
      {key:'408',value:'SAMURAI',keyParent:'61'},
      {key:'409',value:'VITARA',keyParent:'61'},
      {key:'411',value:'SIDEKICK',keyParent:'61'},
      {key:'413',value:'GRAND NOMADE',keyParent:'61'},
      {key:'415',value:'XL7',keyParent:'61'},
      {key:'111',value:'ALTIMA',keyParent:'78'},
      {key:'122',value:'SENTRA II',keyParent:'78'},
      {key:'128',value:'PRIMERA',keyParent:'78'},
      {key:'302',value:'PICK-UP',keyParent:'78'},
      {key:'308',value:'D21 4x4',keyParent:'78'},
      {key:'310',value:'D21',keyParent:'78'},
      {key:'403',value:'PLATINA',keyParent:'78'},
      {key:'408',value:'PATHFINDER',keyParent:'78'},
      {key:'411',value:'NEW PATHFINDER',keyParent:'78'},
      {key:'404',value:'350-Z',keyParent:'78'},
      {key:'405',value:'TIIDA',keyParent:'78'},
      {key:'407',value:'PATROL',keyParent:'78'},
      {key:'409',value:'X-TRAIL',keyParent:'78'},
      {key:'410',value:'MURANO',keyParent:'78'},
      {key:'102',value:'FORZA',keyParent:'61'},
      {key:'104',value:'SWIFT',keyParent:'61'},
      {key:'109',value:'MARUTI',keyParent:'61'},
      {key:'202',value:'WAGON R',keyParent:'61'},
      {key:'301',value:'PICK-UP',keyParent:'61'},
      {key:'402',value:'FURGON',keyParent:'61'},
      {key:'405',value:'SAMURAI PICK-UP',keyParent:'61'},
      {key:'407',value:'APV',keyParent:'61'},
      {key:'410',value:'NOMADE',keyParent:'61'},
      {key:'412',value:'GRAND VITARA',keyParent:'61'},
      {key:'414',value:'JIMNY',keyParent:'61'},
      {key:'205',value:'AEROSTAR',keyParent:'21'},
      {key:'302',value:'F150',keyParent:'21'},
      {key:'309',value:'RANGER 4x4',keyParent:'21'},
      {key:'402',value:'EXPLORER',keyParent:'21'},
      {key:'405',value:'EXPEDITION',keyParent:'21'},
      {key:'408',value:'ECOSPORT',keyParent:'21'},
      {key:'416',value:'CELERIO',keyParent:'61'},
      {key:'114',value:'MARCH',keyParent:'78'},
      {key:'115',value:'MAXIMA',keyParent:'78'},
      {key:'118',value:'SENTRA',keyParent:'78'},
      {key:'121',value:'V16',keyParent:'78'},
      {key:'303',value:'PICK-UP 4x4',keyParent:'78'},
      {key:'309',value:'TERRANO',keyParent:'78'},
      {key:'370',value:'D22',keyParent:'78'},
      {key:'371',value:'D22 4x4',keyParent:'78'},
      {key:'108',value:'ESCORT',keyParent:'21'},
      {key:'125',value:'MONDEO',keyParent:'21'},
      {key:'128',value:'WINDSTAR',keyParent:'21'},
      {key:'206',value:'ECONOLINE',keyParent:'21'},
      {key:'306',value:'RANGER',keyParent:'21'},
      {key:'401',value:'COURIER',keyParent:'21'},
      {key:'404',value:'EXPLORER',keyParent:'21'},
      {key:'406',value:'ESCAPE',keyParent:'21'},
      {key:'110',value:'FIESTA',keyParent:'21'},
      {key:'112',value:'MUSTANG',keyParent:'21'},
      {key:'116',value:'TAURUS',keyParent:'21'},
      {key:'123',value:'EURO ESCORT',keyParent:'21'},
      {key:'126',value:'KA',keyParent:'21'},
      {key:'127',value:'CONTOUR',keyParent:'21'},
      {key:'129',value:'FOCUS',keyParent:'21'},
      {key:'409',value:'EDGE',keyParent:'21'},
      {key:'417',value:'KIZASHI',keyParent:'61'},
      {key:'418',value:'ESCUDO',keyParent:'61'},
      {key:'412',value:'NAVARA',keyParent:'78'},
      {key:'413',value:'QASHQAI',keyParent:'78'},
      {key:'414',value:'TEANA',keyParent:'78'},
      {key:'421',value:'370-Z',keyParent:'78'},
      {key:'428',value:'ROGUE',keyParent:'78'},
      {key:'443',value:'FUSION',keyParent:'21'},
      {key:'421',value:'CIAZ',keyParent:'61'},
      {key:'422',value:'ERTIGA',keyParent:'61'},
      {key:'438',value:'KICKS',keyParent:'78'},
      {key:'420',value:'S-CROSS',keyParent:'61'},
      {key:'430',value:'JUKE',keyParent:'78'},
      {key:'423',value:'RUGBY',keyParent:'61'},
      {key:'419',value:'SWIFT DZIRE',keyParent:'61'},
      {key:'442',value:'F150 RAPTOR',keyParent:'21'},
      {key:'429',value:'VERSA',keyParent:'78'},
      {key:'433',value:'NP 300',keyParent:'78'},
      {key:'428',value:'TRANSIT',keyParent:'21'},
      {key:'434',value:'NOTE',keyParent:'78'},
      {key:'435',value:'NV350',keyParent:'78'},
      {key:'456',value:'CLUB WAGON XLT',keyParent:'21'},
      {key:'426',value:'DZIRE',keyParent:'61'},
      {key:'425',value:'JAZZ',keyParent:'61'},
      {key:'454',value:'CARGO 1119',keyParent:'21'}
    ];
    return modeloTemp
            .filter( val => parseInt(val.keyParent) === parseInt(codMarca) )
            .map( val => <KeyValuePair>{key:val.key,value:val.value} );
  }

}
