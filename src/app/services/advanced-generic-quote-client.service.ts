import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable, throwError} from 'rxjs';
import {JsonResponse} from './model/JsonResponse';
import {Line} from '../core/model/line';
import {catchError} from 'rxjs/operators';
import {Product} from '../core/model/product';
import {QuestionForm} from '../core/model/QuestionForm';
import {Category} from '../core/model/category';

@Injectable({
  providedIn: 'root'
})
export class AdvancedGenericQuoteClientService {

  private static readonly  urlApiAdvancedGenericQuote: string = environment.apiEndpointAdvancedGenericQuote;

  constructor(private http: HttpClient) { }

  public getLines(): Observable<JsonResponse<Line[]>> {
    const METHOD  = '/lines';
    const URL     = AdvancedGenericQuoteClientService.urlApiAdvancedGenericQuote;
    const Http    = this.http.get<JsonResponse<Line[]>>(URL + METHOD, {});
    return Http.pipe(
      catchError(error => {
        return throwError(error);
      })
    );
  }

  public getProducts(lineId: number): Observable<JsonResponse<Product[]>> {
    const METHOD  = '/products';
    const URL     = AdvancedGenericQuoteClientService.urlApiAdvancedGenericQuote;
    const Http    = this.http.get<JsonResponse<Product[]>>(
      URL + METHOD,
      {params: new HttpParams().set('line', lineId + '')}
      );
    return Http.pipe(
      catchError(error => {
        return throwError(error);
      })
    );
  }

  public getQuestions(codLine: string, codProduct: string, codBroker: string): Observable<JsonResponse<QuestionForm>> {
    const METHOD  = '/question';
    const URL     = AdvancedGenericQuoteClientService.urlApiAdvancedGenericQuote;
    const PARAM   = new HttpParams().set('line', codLine)
                                    .set('product', codProduct)
                                    .set('broker', codBroker);
    const Http    = this.http.get<JsonResponse<QuestionForm>>(URL + METHOD, { params: PARAM});
    return Http.pipe(
      catchError(error => {
        return throwError(error);
      })
    );
  }

  public getCategory(codLine: string, codProduct: string, codBroker: string): Observable<JsonResponse<Category[]>> {
    const METHOD  = '/question/category';
    const URL     = AdvancedGenericQuoteClientService.urlApiAdvancedGenericQuote;
    const PARAM   = new HttpParams().set('line', codLine)
                                    .set('product', codProduct)
                                    .set('broker', codBroker);
    const Http    = this.http.get<JsonResponse<Category[]>>(URL + METHOD, { params: PARAM});
    return Http.pipe(
      catchError(error => {
        return throwError(error);
      })
    );
  }

}
