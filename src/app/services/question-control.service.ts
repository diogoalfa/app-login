import { Injectable } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

import { QuestionBase } from '../questions/question-base';
import {requiredChecklist, validatePositiveNum, validateRut} from '../validators/validator-cotgen';


@Injectable()
export class QuestionControlService {
  constructor(public fb: FormBuilder) { }
  toFormGroup(questions: QuestionBase<any>[]) {
    let group: any = {};
    questions.forEach(question => {

      const validatorsList = [];
      if (question.required) { validatorsList.push(Validators.required); }
      if (question.minLength) { validatorsList.push(Validators.minLength(question.minLength)); }
      if (question.maxLength) { validatorsList.push(Validators.maxLength(question.maxLength)); }
      if (question.pattern) { validatorsList.push(Validators.pattern(question.pattern)); }
      if (question['validateRut']) { validatorsList.push(validateRut); }
      if (question['positiveNum']) { validatorsList.push(validatePositiveNum); }

      group[question.key] = new FormControl(question.value || '', Validators.compose(validatorsList));
    });
    return new FormGroup(group);
  }

  toFormGroupForCategory(questions: QuestionBase<any>[]) {
    let groupCategory: FormGroup[] = [];
    let groupControl:any = {};
    questions
      .map(question => question.categoryId)
      .filter((x, i, a) => a.indexOf(x) === i)  // deja los id unicos
      .forEach(categoryId => {
        questions
          .filter(question => question.categoryId === categoryId)
          .forEach(question => {
            const validatorsList = [];
            if (question.required) { validatorsList.push(Validators.required); }
            if (question.minLength) { validatorsList.push(Validators.minLength(question.minLength)); }
            if (question.maxLength) { validatorsList.push(Validators.maxLength(question.maxLength)); }
            if (question['min']) { validatorsList.push(Validators.min(question['min'])); }
            if (question.pattern) { validatorsList.push(Validators.pattern(question.pattern)); }
            if (question['validateRut']) { validatorsList.push(validateRut); }
            if (question['positiveNum']) { validatorsList.push(validatePositiveNum); }
            if (question.controlType === 'checklist') {
              const fgchl: any = {};
              question['items'].forEach(
                item => {
                  fgchl[item.key] = new FormControl(item.value);
                }
              );
              if (question.required) {
                groupControl[question.key] = new FormGroup(fgchl, requiredChecklist);
              } else {
                groupControl[question.key] = new FormGroup(fgchl);
              }
            } else {
              groupControl[question.key] = new FormControl(question.value || '', Validators.compose(validatorsList));
            }
          });
        groupCategory.push(new FormGroup(groupControl));
        groupControl = {};
      });
    return this.fb.group({
      formArray: this.fb.array(groupCategory)
    });
  }

}
