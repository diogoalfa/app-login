export class Poliza {
    id                      : number;
    sseguro                 : number;
    npoliza                 : number;
    fefecto                 : Date;
    fcarpro                 : Date;
    sperson                 : number;
    cpertip                 : number;
    nnumnif                 : string;
    tnombre                 : string;
    tapelli                 : string;
    tdomici                 : string;
    tnomvia                 : string;
    nnumvia                 : string;
    cagente                 : string;
    tnombre_agente          : string;
    tapelli_agente          : string;
    capital_max_maritimo    : number;
    capital_max_aereo       : number;
    capital_max_terrestre   : number;
    prima_minima            : number;
    cmonseg                 : string;
    tmoneda                 : string;
    pcomisi                 : number;
    cciudad                 : number;
    tciudad                 : string;
    cpoblac                 : number;
    tpoblac                 : string;
    cprovin                 : number;
    tprovin                 : string;
    csexper                 : number;

    constructor(
        id,
        sseguro,
        npoliza,
        fefecto,
        fcarpro,
        sperson,
        cpertip,
        nnumnif,
        tnombre,
        tapelli,
        tdomici,
        tnomvia,
        nnumvia,
        cagente,
        tnombre_agente,
        tapelli_agente,
        capital_max_maritimo,
        capital_max_aereo,
        capital_max_terrestre,
        prima_minima,
        cmonseg,
        tmoneda,
        pcomisi,
        cciudad,
        tciudad,
        cpoblac,
        tpoblac,
        cprovin,
        tprovin,
        csexper,
    ) {
        this.id                     = id;
        this.sseguro                = sseguro;
        this.npoliza                = npoliza;
        this.fefecto                = fefecto;
        this.fcarpro                = fcarpro;
        this.sperson                = sperson;
        this.cpertip                = cpertip;
        this.nnumnif                = nnumnif;
        this.tnombre                = tnombre;
        this.tapelli                = tapelli;
        this.tdomici                = tdomici;
        this.tnomvia                = tnomvia;
        this.nnumvia                = nnumvia;
        this.cagente                = cagente;
        this.tnombre_agente         = tnombre_agente;
        this.tapelli_agente         = tapelli_agente;
        this.capital_max_maritimo   = capital_max_maritimo;
        this.capital_max_aereo      = capital_max_aereo;
        this.capital_max_terrestre  = capital_max_terrestre;
        this.prima_minima           = prima_minima;
        this.cmonseg                = cmonseg;
        this.tmoneda                = tmoneda;
        this.pcomisi                = pcomisi;
        this.cciudad                = cciudad;
        this.tciudad                = tciudad;
        this.cpoblac                = cpoblac;
        this.tpoblac                = tpoblac;
        this.cprovin                = cprovin;
        this.tprovin                = tprovin;
        this.csexper                = csexper;
    }
}