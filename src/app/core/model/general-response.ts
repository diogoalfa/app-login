import { BodyResponse } from './body-response';

export class GeneralResponse {
    header  : Object;
    body    : BodyResponse;
    
    constructor(
        header  : Object,
        body    : BodyResponse
    ){
        this.header = header;
        this.body   = body;
    }
}