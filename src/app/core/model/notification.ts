export class Notification {
    icon:           string;
    type:           string;
    textBold:       string;
    textNormal:     string;
    timeAlive:      number;
    closable:       boolean;

    constructor(
        icon,
        type,
        textBold,
        textNormal,
        timeAlive,
        closable
    ) {
        this.icon           = icon;
        this.type           = type;
        this.textBold       = textBold;
        this.textNormal     = textNormal;
        this.timeAlive      = timeAlive;
        this.closable       = closable;
    }
}