export class Restdescription {
  id: number;
  description: string;
  paramKey: string;
  paramValue: string;
  type: string;
}
