export class BodyResponse {
    statusCode  : number;
    message     : string;
    errorData   : string;
    data        : Object;

    constructor(
        statusCode  : number,
        message     : string,
        errorData   : string,
        data        : Object
    ){
        this.statusCode  = statusCode;
        this.message     = message;
        this.errorData   = errorData;
        this.data        = data;
    }
}