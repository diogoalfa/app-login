import {Category} from './category';

export class QuestionInfo {
  id: number;
  label: string;
  paramKey: string;
  category: Category;
}
