import {UserLogin} from './user-login';

export class Session {
  public token: string;
  public user: UserLogin;
}
