export class CodeLineProd {
  codLine:string;
  codProduct:string;

  constructor(codLine: string, codProduct: string) {
    this.codLine = codLine;
    this.codProduct = codProduct;
  }
}
