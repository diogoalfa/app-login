import {QuestionInfo} from './question-info';

export class Paramtree {
  id: number;
  paramKey: string;
  paramValue: string;
  paramTreeParent: Paramtree;
  question: QuestionInfo;
}
