import {Tarifa} from './tarifa';

export class RespCotizacionVehiculo {

  codRetorno: number;
  mensajeRetorno: string;
  detalleResponse: Tarifa[];

}
