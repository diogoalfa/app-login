export class ResponseReactiveForm {
    type        : string;
    message     : string;
    isSuccess   : boolean;

    constructor(){
        this.type       = null;
        this.message    = null;
        this.isSuccess  = false;
    }
}