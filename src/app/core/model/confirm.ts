export class Confirm {
    title       : string;
    message1    : string;
    message2    : string;
    button1     : string;
    button2     : string;

    constructor(
        title,
        message1,
        message2,
        button1,
        button2
    ) {
        this.title      = title;
        this.message1   = message1;
        this.message2   = message2;
        this.button1    = button1;
        this.button2    = button2;
    }
}