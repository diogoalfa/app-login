import {GeneralResponse}    from './general-response';
import { Poliza }           from './poliza';

export class DashboardResponse extends GeneralResponse {
    response    : JSON;
    listPoliza  : Poliza[];
}