export class UserLogin {
  userName: string;
  fullName: string;
  isLoggedin: boolean;
  userCode: string;
}
