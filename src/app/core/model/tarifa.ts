export class Tarifa {

  codProducto: number;
  descProducto: string;
  codPlan: number;
  descPlan: string;
  codDeducible: number;
  descDeducible: string;
  nroCotizacion: number;
  porcentajeDescuento: number;
  porcentajeRecargo: number;
  porcentajeComision: number;
  primaAfecta: number;
  primaExenta: number;
  primaBruta: number;
  primaNeta: number;
  mapCoberturas?: any;

}
