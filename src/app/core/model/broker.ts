export class Broker {
/*
"id": 1,
        "username": "C_LOTA",
        "code": "07965",
        "sperson": "347663",
        "lastName": "SOC. CORREDORES DE SEGS.LOTA Y ASOCIADOS LTDA.",
        "name": null,
        "codSucursal": "010",
        "nameSucursal": "Hendaya",
        "mail": null,
        "fullName": "SOC. CORREDORES DE SEGS.LOTA Y ASOCIADOS LTDA."
 */
 id: number;
 username: string;
 code: string;
 sperson: number;
 lastName: string;
 name: string;
 codSucursal: string;
 nameSucursal: string;
 mail: string;
 fullName: string;
}
