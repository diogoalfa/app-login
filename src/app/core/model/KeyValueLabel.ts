interface KeyValueLabel {
  key: string;
  value: boolean;
  label: string;
}
