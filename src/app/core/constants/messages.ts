export const MESSAGES = {

    TITLE: {
        ERROR           : 'ERROR',
        ERROR_CONEXION  : 'ERROR DE CONEXIÓN',
        INFO            : 'INFORMACIÓN',
        WARNING         : 'IMPORTANTE'
    },

    ERROR: {
        COMMON: {
            SERVICIO_NO_DISPONIBLE      : 'Servicio no disponible',
            DATOS_INCOMPLETOS           : 'Datos incompletos',
            CAMPO_REQUERIDO             : 'Campo requerido',
            DATOS_ERRONEOS              : 'Algunos datos presentan errores. Verifique campos en rojo',
            OPERACION_NO_REALIZADA      : 'No se ha podido realizar la operación',
            MENOR_IGUAL_CERO            : 'Debe ser mayor o igual a 0',
            MENOR_IGUAL_NOVENTA_NUEVE   : 'Debe ser menor o igual a 99',
            NO_EXISTE_VALOR_CAMBIO      : 'No existe valor de cambio para moneda el dia de hoy'
        },

        MONTO_ASEGURADO : {
            MONTO_MINIMO  : 'El monto no debe ser menor al configurado en el marco de la póliza.',
            MONTO_MAXIMO  : 'El monto no debe ser mayor al configurado en el marco de la póliza.',
        },

        EMISION: {
            CERTIFICADO: {
                GUARDAR_EMISION     : 'En estos momentos no podemos guardar la información, intenta más tarde.',
                GENERAR_CERTIFICADO : 'En este momento no podemos emitir el certificado. Intenta más tarde.'
            }
        },

        CALCULO_PRIMA:{
            NO_REALIZADO : 'En estos momentos no podemos calcular la prima, intente más tarde.',
        }
    },

    SUCCESS: {
        COMMON: {
            OPERACION_REALIZADA     : 'Operación Realizada',
            SIN_RESULTADOS          : 'La búsqueda no arrojó resultados.',
            GRABADO_EXITOSAMENTE    : 'La información ha sido grabada exitosamente'
        },

        EMISION: {
            CERTIFICADO: {
                GENERAR_CERTIFICADO : 'El certificado ha sido emitido correctamente.'
            }
        },
    },

    LOGIN: {
        REQUERIDO   : 'Introduzca su nombre de usuario y contraseña',
        FALLO       : 'No se pudo realizar la autenticación, por favor verifique los datos e intente nuevamente.'
    },

    DASHBOARD: {
        LISTADO: {
            VACIO   : 'Para mayor información de nuestros productos, contáctenos al 600 542 3789.',
            FALLO   : 'Ocurrió un error al intentar obtener el listado de pólizas vigentes. Por favor, intente más tarde.'
        }
    },

    CONFIRM: {
        EMISION: {
            CERTIFICADO : 'Desea continuar con la emisión del certificado?',
        },
        CANCELAR : {
            DESEA_CANCELAR  : 'Desea cancelar de todos modos?',
            EMISION         : 'Está seguro de cancelar la emisión del certificado?'
        }
    },

    INFO: {
        COMMON: {
            PEDER_CAMBIOS: 'Perderá los cambios realizados.'
        }
    }
};
