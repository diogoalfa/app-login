export const CONFIGS = {
    //CONFIGURACION LIBERTY
    NOMBRE_EMPRESA  : 'LIBERTY SEGUROS',
    WEB_EMPRESA     : 'https://www.liberty.cl/',
    
    //URL API RESTFull
    URL_API_EMISION : 'https://desarrollo.liberty.cl/TransporteAPI',
    //URL_API_EMISION : 'http://localhost:8080/TransporteAPI',

    SEPARADOR:{
        DECIMAL : ",",
        MILES   : "."
    },
    
    CANTIDAD : {
        DECIMAL : 2
    },
    
    TRANSPORTE : {
        MARITIMO    : {
            NACIONAL        : "M.N",
            INTERNACIONAL   : "M.L",
            IMPORTACION     : "M.I",
            EXPORTACION     : "M.E",
            CABOTAJE        : "M.C"
        },
        AEREO : {
            NACIONAL        : "A.N",
            INTERNACIONAL   : "A.L",
            IMPORTACION     : "A.I",
            EXPORTACION     : "A.E",
            CABOTAJE        : "A.C"
        },
        TERRESTRE : {
            NACIONAL        : "T.N",
            INTERNACIONAL   : "T.L",
            IMPORTACION     : "T.I",
            EXPORTACION     : "T.E",
            CABOTAJE        : "T.C"
        }
    }
 };