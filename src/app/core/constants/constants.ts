export const CONSTANTS = {
    // FORMATOS DE CAMPOS
    DATE_FORMAT         : 'dd/MM/yyyy',
    DATE_FORMAT_24H     : 'dd/MM/yyyy HH:mm',

    BUTTON: {
        SI              : 'Sí',
        NO              : 'No',
        CONTINUAR       : 'Continuar',
        CANCELAR        : 'Cancelar',
        GUARDAR         : 'Guardar',
        EMITIR          : 'Emitir',
        CALCULAR_PRIMA  : 'Calcular Prima'
    },

    TITLE: {
        CONFIRM : {
            EMITIR_CERTIFICADO  : 'Emitir Certificado',
            CANCELAR_GUARDADO   : 'Cancelar operación'
        }
    },

    SYMBOL: {
        CURRENCY: {
            CLP         : '$ CLP',
            USD         : '$ UD',
            UF          : 'UF'
        }
    },

    NOTIFICATION: {
        ICON: {
            INFO:       'announcement',
            SUCCESS:    'check',
            WARNING:    'warning',
            DANGER:     'cancel'
        },
        TYPE: {
            SUCCESS:    'success',
            ERROR:      'primary',
            DANGER:     'danger',
            INFO:       'info',
            WARNING:    'warning',
        }
    },

    COUNTRY_DEFAULT: {
        CHILE: {
            cod : '152',
            des : 'Chile'
        }
    }

};
