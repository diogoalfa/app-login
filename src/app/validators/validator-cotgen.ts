import {AbstractControl, FormGroup, ValidationErrors} from '@angular/forms';
import * as RutHelper from 'rut-helpers';



export function validateRut(control: AbstractControl): ValidationErrors | null {
  if (!RutHelper.rutValidate(control.value)) {
    return { validateRut: true };
  }
  return null;
}


export function validatePositiveNum(control: AbstractControl): ValidationErrors | null {
  if (isNaN(control.value)) {
    return {pattern: true};
  } else if (parseFloat(control.value) < 1) {
    return {positiveNum: true};
  }
  return null;
}

export function requiredChecklist(fg: FormGroup): ValidationErrors | null {

  let valid = false;
  Object.keys(fg.value).forEach(
    fgKey => {
      if (fg.value[fgKey]) {
        valid = fg.value[fgKey];
        return;
      }
    }
  );

  if (valid) {
    return null;
  } else {
    return {required: true};
  }

}
