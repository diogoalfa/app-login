import {QuestionBase} from "./question-base";

export class QuestionRut extends QuestionBase<String>{
  controlType = 'rut';
  type: string;
  validateRut: boolean;
  constructor(options: {} = {}) {
    super(options);
    this.type = options['type'] || '';
    this.validateRut = options['validateRut'];
  }
}
