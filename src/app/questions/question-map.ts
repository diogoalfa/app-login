import {QuestionBase} from './question-base';

export class MapQuestion extends QuestionBase<string> {

  controlType = 'map';
  type: string;

}
