import {QuestionBase} from './question-base';

export class RadioQuestion extends QuestionBase<string> {

  controlType = 'radio';
  type: string;
  items: KeyValueLabel[] = [];

  constructor(options: {} = {}) {
    super(options);
    this.type = options['type'] || '';
    this.items = options['items'];
  }

}
