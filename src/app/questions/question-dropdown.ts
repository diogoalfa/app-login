import { QuestionBase } from './question-base';

export class DropdownQuestion extends QuestionBase<string> {
  controlType = 'dropdown';
  options: KeyValuePair[] = [];
  entityName:string;

  constructor(options: {} = {}) {
    super(options);
    this.options = options['options'] || [];
    this.entityName = options['entityName'] || '';
  }
}
