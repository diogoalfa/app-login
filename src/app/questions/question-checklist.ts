import {QuestionBase} from './question-base';

export class ChecklistQuestion extends QuestionBase<string> {
  controlType = 'checklist';
  type: string;
  items: KeyValueLabel[] = [];

  constructor(options: {} = {}) {
    super(options);
    this.type = options['type'] || '';
    this.items = options['items'];
  }
}
