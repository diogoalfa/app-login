export class QuestionBase<T> {
    value: T;
    key: string;
    label: string;
    required: boolean;
    order: number;
    controlType: string;
    categoryId: number;
    entityName: string;
    minLength: number;
    maxLength: number;
    pattern: string;
    options: any[];
    display: any[];
    depend: string;

    constructor(options: {
        value?: T,
        key?: string,
        label?: string,
        required?: boolean,
        order?: number,
        controlType?: string,
        categoryId?: number,
        entityName?: string,
        minLength?: number,
        maxLength?: number,
        pattern?: string,
        options?: any[],
        display?: any[],
        depend?: string
    } = {}) {
      this.value = options.value;
      this.key = options.key || '';
      this.label = options.label || '';
      this.required = !!options.required;
      this.order = options.order === undefined ? 1 : options.order;
      this.controlType = options.controlType || '';
      this.categoryId = options.categoryId;
      this.entityName = options.entityName || '';
      this.minLength = options.minLength;
      this.maxLength = options.maxLength;
      this.pattern = options.pattern;
      this.options = options.options || [];
      this.display = options.display;
      this.depend = options.depend;
    }
  }
