import { QuestionBase } from './question-base';

export class TextboxQuestion extends QuestionBase<string> {
  controlType = 'textbox';
  type: string;
  positiveNum: boolean;
  min: number;

  constructor(options: {} = {}) {
    super(options);
    this.type = options['type'] || '';
    this.positiveNum = options['positiveNum'];
    this.min = options['min'];
  }
}
