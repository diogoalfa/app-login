import {QuestionBase} from "./question-base";

export class QuestionSelectChild extends QuestionBase<string>{
  controlType = 'selectchild';
  constructor(options: {} = {}) {
    super(options);
  }
}
