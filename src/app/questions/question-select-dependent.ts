import {QuestionBase} from "./question-base";

/**
 * Question para enlazar select option dependientes
 */
export class QuestionSelectDependent extends QuestionBase<string>{
  controlType = 'selectdependent';
  options: {key: string, value: string}[] = [];
  isParent:boolean;
  keyChild: string;
  isChild:boolean;
  keyParent:string;
  entityNameChild:string;

  constructor(options: {} = {}) {
    super(options);
    this.options = options['options'] || [];
    this.isParent = options['isParent'] || [];
    this.keyChild = options['keyChild'] || '';
    this.isChild = options['isChild'] || '';
    this.keyParent = options['keyParent'] || '';
    this.entityNameChild = options['entityNameChild'] || '';
  }
}
