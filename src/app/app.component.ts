import { Component } from '@angular/core';
import { QuestionService } from './services/question.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [QuestionService]
})
export class AppComponent {
  questions: any[];

  constructor(private qs: QuestionService) {
    this.questions = [];
  }

  loadQuestion(questions: any[]) {
    console.log("loadQuestion()");
    console.log("$event question:");
    console.log(questions);
    console.log("questionsOut size: ",this.questions.length);
    this.questions = questions;
    console.log("questionsOut2 size: ",this.questions.length);
  }

}
