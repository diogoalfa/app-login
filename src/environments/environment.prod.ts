export const environment = {
  production: true,
  apiEndpointCotizadorVehiculos: 'https://servicios-test.liberty.cl/CotizadorGenericoLibertyWS/rest/cotizaciones/vehiculos',
  apiEndpointAdvancedGenericQuote: 'http://localhost:8080/advanced-generic-quote/v1.0.0'
};
